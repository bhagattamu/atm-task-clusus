package com.progressoft.induction.atm;

import com.progressoft.iduction.model.Account;
import com.progressoft.iduction.model.Note;
import com.progressoft.induction.atm.exceptions.AccountNotFoundException;
import com.progressoft.induction.atm.exceptions.NotEnoughMoneyInATMException;
import com.progressoft.induction.impl.ATMImpl;
import com.progressoft.induction.impl.BankingSystemImpl;
import com.progressoft.induction.atm.exceptions.InsufficientFundsException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


class ATMTest {

    private ATM atm;

    @BeforeEach
    void setUp() {
    	BankingSystem bSystem;
    	
    	// data inserted in map of accounts
    	Map<String, Account> accounts = new HashMap<String, Account>();
    	accounts.put("123456789", new Account.Builder("123456789").withBalance(new BigDecimal(1000.0)).build());
    	accounts.put("111111111", new Account.Builder("111111111").withBalance(new BigDecimal(1000.0)).build());
    	accounts.put("222222222", new Account.Builder("222222222").withBalance(new BigDecimal(1000.0)).build());
    	accounts.put("333333333", new Account.Builder("333333333").withBalance(new BigDecimal(1000.0)).build());
    	accounts.put("444444444", new Account.Builder("444444444").withBalance(new BigDecimal(1000.0)).build());
    	bSystem = new BankingSystemImpl(accounts);
    	
    	// data inserted in map of Notes
    	Map<Banknote, Note> atmNotes = new HashMap<Banknote, Note>();
    	Note fiveNote = new Note.NoteBuilder(Banknote.FIVE_JOD).withNumber(100).build();
    	Note tenNote = new Note.NoteBuilder(Banknote.TEN_JOD).withNumber(100).build();
    	Note twentyNote = new Note.NoteBuilder(Banknote.TWENTY_JOD).withNumber(20).build();
    	Note fiftyNote = new Note.NoteBuilder(Banknote.FIFTY_JOD).withNumber(10).build();
    	atmNotes.put(Banknote.FIVE_JOD, fiveNote);
    	atmNotes.put(Banknote.TEN_JOD, tenNote);
    	atmNotes.put(Banknote.TWENTY_JOD, twentyNote);
    	atmNotes.put(Banknote.FIFTY_JOD, fiftyNote);
    	
    	// init atm using constructor
        atm = new ATMImpl(atmNotes, bSystem);
    }

    @Test
    void givenAccountNumberThatDoesNotExist_whenWithdraw_thenShouldThrowException() {
        Assertions.assertThrows(AccountNotFoundException.class,
                () -> atm.withdraw("14141414141", new BigDecimal("120.0")));
    }

    @Test
    void givenValidAccountNumber_whenWithdrawAmountLargerThanTheAccountBalance_thenShouldThrowException() {
        Assertions.assertThrows(InsufficientFundsException.class,
                () -> atm.withdraw("123456789", new BigDecimal("20000.0")));
    }

    @Test
    void whenWithdrawAmountLargerThanWhatInMachine_thenShouldThrowException() {
        atm.withdraw("123456789", new BigDecimal("1000.0"));
        atm.withdraw("111111111", new BigDecimal("1000.0"));

        Assertions.assertThrows(NotEnoughMoneyInATMException.class,
                () -> atm.withdraw("444444444", new BigDecimal("500.0")));
    }

    @Test
    void whenWithdraw_thenSumOfReceivedBanknotesShouldEqualRequestedAmount() {
        BigDecimal requestedAmount = new BigDecimal(700);
        List<Banknote> receivedBanknotes = atm.withdraw("111111111", requestedAmount);

        BigDecimal sumOfAllBanknotes = receivedBanknotes.stream().map(Banknote::getValue).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);

        Assertions.assertEquals(sumOfAllBanknotes.compareTo(requestedAmount), 0);
    }

    @Test
    void givenAllFundsInAccountAreWithdrwan_whenWithdraw_shouldThrowException() {
        atm.withdraw("222222222", new BigDecimal("500"));
        atm.withdraw("222222222", new BigDecimal("500"));

        Assertions.assertThrows(InsufficientFundsException.class,
                () -> atm.withdraw("222222222", new BigDecimal("500")));
    }
}