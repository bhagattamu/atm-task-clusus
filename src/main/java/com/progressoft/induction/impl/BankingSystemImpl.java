package com.progressoft.induction.impl;

import java.math.BigDecimal;
import java.util.Map;

import com.progressoft.iduction.model.Account;
import com.progressoft.induction.atm.BankingSystem;
import com.progressoft.induction.atm.exceptions.AccountNotFoundException;

public class BankingSystemImpl implements BankingSystem {
	private Map<String, Account> accounts;
	
	public BankingSystemImpl(Map<String, Account> accounts) {
		super();
		this.accounts = accounts;
	}

	@Override
	public BigDecimal getAccountBalance(String accountNumber) {
		if(this.accounts.get(accountNumber) == null) {
			// account not found
			throw new AccountNotFoundException();
		}
		return this.accounts.get(accountNumber).getBalance();
	}

	@Override
	public void debitAccount(String accountNumber, BigDecimal amount) {
		// subtracting withdrawn amount from account balance 
		Account account = this.accounts.get(accountNumber);
		account.setBalance(account.getBalance().subtract(amount));
		this.accounts.put(accountNumber, account);
	}

}
