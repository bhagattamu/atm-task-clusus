package com.progressoft.induction.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.progressoft.iduction.model.Note;
import com.progressoft.induction.atm.ATM;
import com.progressoft.induction.atm.BankingSystem;
import com.progressoft.induction.atm.Banknote;
import com.progressoft.induction.atm.exceptions.InsufficientFundsException;
import com.progressoft.induction.atm.exceptions.NotEnoughMoneyInATMException;

public class ATMImpl implements ATM {
	
	private Map<Banknote, Note> atmNotes;
	private BankingSystem bSystem;
	
	public ATMImpl(Map<Banknote, Note> atmNotes, BankingSystem bSystem) {
		super();
		this.atmNotes = atmNotes;
		this.bSystem = bSystem;
	}
	
	public BigDecimal getTotalATMCash() {
		
		BigDecimal totalCash = new BigDecimal(0);
		
		for (Map.Entry<Banknote, Note> entry : this.atmNotes.entrySet()) {             		
    		// adding total cash to the multiple of number and cash to get actual total cash in end
			totalCash = totalCash.add(entry.getValue().getNote().getValue().multiply(new BigDecimal(entry.getValue().getNumber())));
    	}		
		return totalCash;
	}
	
	public List<Banknote> generateCashList (BigDecimal amount){
		
		List<Banknote> cashList = new ArrayList<Banknote>();
		BigDecimal sumOfAllBanknotes = new BigDecimal(0);
		Banknote nextNote = Banknote.FIVE_JOD;
		
		// Number of notes of respective note
		int fiveNoteNumber = this.atmNotes.get(Banknote.FIVE_JOD).getNumber();
		int tenNoteNumber = this.atmNotes.get(Banknote.TEN_JOD).getNumber();
		int twentyNoteNumber = this.atmNotes.get(Banknote.TWENTY_JOD).getNumber();
		int fiftyNoteNumber = this.atmNotes.get(Banknote.FIFTY_JOD).getNumber();

		// Remainder of 5 = 0 means have cash that is inside ATM
		if(amount.remainder(new BigDecimal(5)).compareTo(new BigDecimal(0)) == 0) {
			while(sumOfAllBanknotes.compareTo(amount) < 0) {
				
				if(fiveNoteNumber == 0 && tenNoteNumber == 0 && twentyNoteNumber == 0 && fiftyNoteNumber == 0) {									
					// note in atm is empty
					return null;					
				}
				
				switch(nextNote) {
					case FIVE_JOD:
						// check if note added exceed the withdrawn amount
						if(sumOfAllBanknotes.add(Banknote.FIVE_JOD.getValue()).compareTo(amount) == 1) { 
							// sum greater than amount
							throw new NotEnoughMoneyInATMException();
						}else {
							// five note finish
							if(fiveNoteNumber == 0) { 
								nextNote = Banknote.TEN_JOD;
								break;
							}
							sumOfAllBanknotes = sumOfAllBanknotes.add(Banknote.FIVE_JOD.getValue());
							nextNote = Banknote.TEN_JOD;
							cashList.add(Banknote.FIVE_JOD);
							fiveNoteNumber -= 1; 
						}						
						break;
					case TEN_JOD:
						if(sumOfAllBanknotes.add(Banknote.TEN_JOD.getValue()).compareTo(amount) == 1) {
							nextNote = Banknote.TWENTY_JOD;
						}else {
							// ten note finish
							if(tenNoteNumber == 0) { 								
								nextNote = Banknote.TWENTY_JOD;
								break;
							}
							sumOfAllBanknotes = sumOfAllBanknotes.add(Banknote.TEN_JOD.getValue());
							nextNote = Banknote.TWENTY_JOD;
							cashList.add(Banknote.TEN_JOD);	
							tenNoteNumber -= 1;
						}
						break;
					case TWENTY_JOD:
						if(sumOfAllBanknotes.add(Banknote.TWENTY_JOD.getValue()).compareTo(amount) == 1) {
							nextNote = Banknote.FIFTY_JOD;
						}else {
							// twenty note finish
							if(twentyNoteNumber == 0) { 							
								nextNote = Banknote.FIFTY_JOD;
								break;
							}
							sumOfAllBanknotes = sumOfAllBanknotes.add(Banknote.TWENTY_JOD.getValue());
							nextNote = Banknote.FIFTY_JOD;
							cashList.add(Banknote.TWENTY_JOD);
							twentyNoteNumber -= 1;
						}
						break;
					case FIFTY_JOD:
						if(sumOfAllBanknotes.add(Banknote.FIFTY_JOD.getValue()).compareTo(amount) == 1) {
							nextNote = Banknote.FIVE_JOD;
						}else {
							// fifty note finish
							if(fiftyNoteNumber == 0) { 								
								nextNote = Banknote.FIVE_JOD;
								break;
							}
							sumOfAllBanknotes = sumOfAllBanknotes.add(Banknote.FIFTY_JOD.getValue());
							nextNote = Banknote.FIVE_JOD;
							cashList.add(Banknote.FIFTY_JOD);
							fiftyNoteNumber -= 1;
						}
						break;
					default:
						break;
				}				
			}
		}
		else {
			return null;	
		}
		return cashList;
	}
	
	@Override
	public List<Banknote> withdraw(String accountNumber, BigDecimal amount) {
		
		// List of cash i.e. withdrawn from atm
		List<Banknote> withdrawNotes = new ArrayList<Banknote>();
		// Total balance in account of respective account number
		BigDecimal totalBalance = bSystem.getAccountBalance(accountNumber);		
		
		// check whether Account has min balance i.e. required to withdraw from it
		if(amount.compareTo(totalBalance) < 1) {			
			// check whether ATM has min balance i.e. required to withdraw from it
			if(amount.compareTo(getTotalATMCash()) < 1) {				
				// i.e. atm have balance to withdraw
				withdrawNotes = generateCashList(amount);
				
				if(withdrawNotes == null) {					
					// not enough money or cash not available for this amount
					throw new NotEnoughMoneyInATMException();					
				}else {					
					bSystem.debitAccount(accountNumber, amount);					
					// note deduct from atm i.e. taken by client
					withdrawNotes.forEach(note -> {								
						int number = this.atmNotes.get(note).getNumber();
						Note noteInstance = this.atmNotes.get(note);
						noteInstance.setNumber(number - 1);
						this.atmNotes.put(note, noteInstance);
					});					
				}				
			}else { // Not enough money in ATM				
				throw new NotEnoughMoneyInATMException();				
			}
		}else { // Insufficient money in account			
			throw new InsufficientFundsException();			
		}
		return withdrawNotes;
	}
}
