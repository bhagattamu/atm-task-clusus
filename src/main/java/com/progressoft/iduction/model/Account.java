package com.progressoft.iduction.model;

import java.math.BigDecimal;

public class Account {
	private String accountNumber;
	private BigDecimal balance;
	
	private Account() {
		
	}
	
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public static class Builder{
		private String accountNumber;
		private BigDecimal balance;
		
		public Builder(String accountNumber) {
			this.accountNumber = accountNumber;
		}
		
		public Builder withBalance(BigDecimal balance) {
			this.balance = balance;
			return this;
		}
		
		public Account build() {
			Account account = new Account();
			account.accountNumber = this.accountNumber;
			account.balance = this.balance;
			return account;
		}
	}
	
}
