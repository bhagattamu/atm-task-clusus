package com.progressoft.iduction.model;

import com.progressoft.induction.atm.Banknote;

public class Note {
	private Banknote note;
	private int number;
	
	private Note() {
		
	}
	
	public Banknote getNote() {
		return note;
	}

	public void setNote(Banknote note) {
		this.note = note;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public static class NoteBuilder {
		private Banknote note;
		private int number;
		
		public NoteBuilder(Banknote note) {
			this.note = note;
		}
		
		public NoteBuilder withNumber(int number) {
			this.number = number;
			return this;
		}
		
		public Note build() {
			Note note = new Note();
			note.note = this.note;
			note.number = this.number;
			return note;
		}
	}
}
